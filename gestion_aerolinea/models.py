from django.db import models
from django.db.models.base import Model
from django.db.models.deletion import CASCADE
from django.utils import timezone

# Create your models here.

class Vuelo(models.Model):
    id_vuelo=models.AutoField(primary_key=True)
    destino=models.CharField(max_length=15)
    avion=models.CharField(max_length=15)
    costo=models.IntegerField()
    oferta= models.DecimalField(null=True, max_digits = 3, decimal_places = 2)
    imagen_destino= models.ImageField(upload_to="photos")
    sillas=models.IntegerField()
    fecha=models.DateField()

class Usuario(models.Model):
    id_usuario=models.AutoField(primary_key=True)
    cedula = models.IntegerField()
    nombre = models.CharField(max_length=30)
    edad = models.IntegerField()
    cuenta_bancaria = models.IntegerField()

class Compra(models.Model):
   id_compra=models.AutoField(primary_key=True)
   id_vuelo=models.ForeignKey(Vuelo, on_delete=models.CASCADE)
   id_usuario=models.ForeignKey(Usuario, on_delete=models.CASCADE)
   fecha_compra=models.DateField()

class Pelicula(models.Model):
   id_pelicula=models.AutoField(primary_key=True)
   id_compra=models.ForeignKey(Compra, on_delete=models.CASCADE)
   categoria=models.CharField('categoria', max_length=255)
   anho=models.IntegerField()
   genero=models.CharField('genero', max_length=255)

class Reembolso(models.Model):
   id_reembolso=models.AutoField(primary_key=True)
   id_usuario=models.ForeignKey(Usuario, on_delete=models.CASCADE)
   id_compra=models.ForeignKey(Compra, on_delete=models.CASCADE)
   tipo_de_reembolso=models.CharField('Tipo', max_length=255)
   motivo_cancelacion=models.CharField('Motivo', max_length=255)
   aprobacion=models.BooleanField()
