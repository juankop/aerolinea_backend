from django.contrib import admin
from .models import Vuelo, Usuario, Pelicula, Reembolso, Compra

# Register your models here.


admin.site.register(Pelicula)
admin.site.register(Usuario)
admin.site.register(Vuelo)
admin.site.register(Compra)
admin.site.register(Reembolso)