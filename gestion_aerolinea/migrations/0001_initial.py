# Generated by Django 3.2.8 on 2021-10-13 02:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Avion',
            fields=[
                ('id_avion', models.AutoField(primary_key=True, serialize=False)),
                ('sillas', models.IntegerField()),
                ('matricula', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id_cliente', models.AutoField(primary_key=True, serialize=False)),
                ('num_cuenta', models.IntegerField()),
                ('cedula', models.CharField(max_length=15)),
                ('nombre', models.CharField(max_length=15)),
                ('apellido', models.CharField(max_length=15)),
                ('fecha_nacimiento', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Compra',
            fields=[
                ('id_compra', models.AutoField(primary_key=True, serialize=False)),
                ('fecha_compra', models.DateField()),
                ('id_cliente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.cliente')),
            ],
        ),
        migrations.CreateModel(
            name='Destino',
            fields=[
                ('id_destino', models.AutoField(primary_key=True, serialize=False)),
                ('ciudad', models.CharField(max_length=15)),
                ('pais', models.CharField(max_length=15)),
            ],
        ),
        migrations.CreateModel(
            name='Origen',
            fields=[
                ('id_origen', models.AutoField(primary_key=True, serialize=False)),
                ('ciudad', models.CharField(max_length=15)),
                ('pais', models.CharField(max_length=15)),
            ],
        ),
        migrations.CreateModel(
            name='Pelicula',
            fields=[
                ('id_pelicula', models.AutoField(primary_key=True, serialize=False)),
                ('categoria', models.CharField(max_length=15)),
                ('genero', models.CharField(max_length=15)),
                ('año', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Ruta',
            fields=[
                ('id_ruta', models.AutoField(primary_key=True, serialize=False)),
                ('id_destino', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.destino')),
                ('id_origen', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.origen')),
            ],
        ),
        migrations.CreateModel(
            name='Vuelo_disponible',
            fields=[
                ('id_vuelodisponible', models.AutoField(primary_key=True, serialize=False)),
                ('fecha', models.DateField()),
                ('id_ruta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.ruta')),
            ],
        ),
        migrations.CreateModel(
            name='Vuelo_asignado',
            fields=[
                ('id_vueloasignado', models.AutoField(primary_key=True, serialize=False)),
                ('costo', models.IntegerField()),
                ('id_avion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.avion')),
                ('id_vuelodisponible', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.vuelo_disponible')),
            ],
        ),
        migrations.CreateModel(
            name='Reembolso',
            fields=[
                ('id_reembolso', models.AutoField(primary_key=True, serialize=False)),
                ('tipo_reembolso', models.CharField(max_length=15)),
                ('tipo_motivo', models.CharField(max_length=15)),
                ('estado_aprobacion', models.BooleanField()),
                ('id_compra', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.compra')),
            ],
        ),
        migrations.CreateModel(
            name='Oferta',
            fields=[
                ('id_oferta', models.AutoField(primary_key=True, serialize=False)),
                ('porcentaje_descuento', models.DecimalField(decimal_places=2, max_digits=3)),
                ('id_vueloasignado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.vuelo_asignado')),
            ],
        ),
        migrations.AddField(
            model_name='compra',
            name='id_oferta',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.oferta'),
        ),
        migrations.AddField(
            model_name='compra',
            name='id_vueloasignado',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.vuelo_asignado'),
        ),
        migrations.AddField(
            model_name='avion',
            name='id_pelicula',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestion_aerolinea.pelicula'),
        ),
    ]
